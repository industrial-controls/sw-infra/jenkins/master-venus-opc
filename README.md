# Jenkins Master - Venus OPC - CASC

Jenkins instance for the Venus OPC simulation framework

## Run locally

### Build
```
docker build -t jenkins-venus-opc .
```

### Configure
Configure `./deployments/master.env` with backup repo url and git name & email config.
Generate PAT with write access to backup repo url and put it in `./secrets/secrets.env`.

### Run
```
mkdir -m 777 /tmp/jenkins_home
docker run -p 8080:8080 \
    -v /tmp/jenkins_home:/var/jenkins_home:z \
    -v $PWD/casc:/var/lib/jenkins/casc_configs \
    --env-file ./deployment/master.env \
    --env-file ./secrets/secrets.env \
    jenkins-venus-opc
```