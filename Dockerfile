FROM gitlab-registry.cern.ch/industrial-controls/sw-infra/jenkins/master-master:2.277

USER root

RUN sudo yum install -y groovy

RUN /usr/local/bin/install-plugins.sh docker-plugin:1.2.2 groovy:2.4 timestamper:1.13 openstack-cloud:2.58

USER jenkins

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/libexec/run"]
